#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Tile {
    Visited(u32),
    Unvisited,
}
