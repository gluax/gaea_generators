use std::ops::{Deref, DerefMut};

use super::Tile;

/// Base tile map
#[derive(Clone, Debug)]
pub struct TileMap {
    pub height: u16,
    pub width:  u16,
    pub map:    Vec<Vec<Tile>>,
}

impl TileMap {
    pub fn new(width: u16, height: u16) -> Self {
        let map = (0..height)
            .into_iter()
            .map(|_| (0..width).into_iter().map(|_| Tile::Unvisited).collect())
            .collect();
        Self { height, width, map }
    }
}

impl Deref for TileMap {
    type Target = Vec<Vec<Tile>>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}

impl DerefMut for TileMap {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.map
    }
}
