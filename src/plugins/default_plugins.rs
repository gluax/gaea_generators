use bevy::{
    app::{PluginGroup, PluginGroupBuilder, ScheduleRunnerPlugin},
    core::CorePlugin,
    input::InputPlugin,
    window::WindowPlugin,
};

pub struct DefaultPlugins;

impl PluginGroup for DefaultPlugins {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group.add(CorePlugin::default());
        group.add(InputPlugin::default());
        group.add(ScheduleRunnerPlugin::default());
        group.add(WindowPlugin::default());
        group.add(bevy::winit::WinitPlugin::default());

        #[cfg(feature = "debug")]
        {
            group.add(bevy::asset::AssetPlugin::default());
            group.add(bevy::render::RenderPlugin::default());
            group.add(bevy::core_pipeline::CorePipelinePlugin::default());
            group.add(bevy::sprite::SpritePlugin::default());
            group.add(bevy::text::TextPlugin::default());
            group.add(bevy::ui::UiPlugin::default());
            group.add(bevy::log::LogPlugin::default());
            group.add(bevy::diagnostic::DiagnosticsPlugin::default());
            group.add(bevy::diagnostic::FrameTimeDiagnosticsPlugin::default());
            group.add(bevy::diagnostic::LogDiagnosticsPlugin::default());
            group.add(bevy_inspector_egui::WorldInspectorPlugin::new());
        }
    }
}
