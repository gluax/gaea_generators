use bevy::{
    app::{App, ScheduleRunnerSettings},
    prelude::Plugin,
    utils::Duration,
    window::WindowDescriptor,
};

use super::default_plugins::DefaultPlugins;
use crate::systems::camera_setup_system;

pub struct GaeaGeneratorsPlugin;

impl Plugin for GaeaGeneratorsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ScheduleRunnerSettings::run_loop(Duration::from_secs_f64(
            1.0,
        )))
        .insert_resource(WindowDescriptor {
            title: "Gaea Generators".to_string(),
            width: 500.,
            height: 300.,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_startup_system(camera_setup_system);
    }
}
