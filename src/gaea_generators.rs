#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod components;
mod plugins;
mod systems;
mod tilemap;
mod traits;

use bevy::app::App;

use crate::plugins::GaeaGeneratorsPlugin;

fn main() {
    let mut app = App::new();

    app.add_plugin(GaeaGeneratorsPlugin);

    app.run();
}
