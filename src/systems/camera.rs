use bevy::prelude::{Commands, OrthographicCameraBundle};

pub fn camera_setup_system(mut commands: Commands) {
    // 2D orthographic camera
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}
